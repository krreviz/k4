import java.util.*;


/** This class represents fractions of form n/d where n and d are long integer
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      // TODO!!! Your debugging tests here
   }
   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */

   public Lfraction (long a, long b) {
      if (a < 0 && b < 0) {
         a = -a;
         b = -b;
      }
      if (b <= 0) {
         throw new RuntimeException("can't create! Cause: " + b + " in " + this);
      }
      long gcdMain = gcd(a, b);
      a = a / gcdMain;
      b = b / gcdMain;
      numerator = a;
      denominator = b;
   }

   static long gcd(long numerator, long denominator) {
      if ( numerator == 0)
         if (denominator < 0) {
            return -denominator;
         } else return denominator;
      return gcd(denominator % numerator, numerator);
   }

   /** Public method to access the numerator field.
    * @return numerator
    */
   public long getNumerator() {
      return this.numerator;
   }

   /** Public method to access the denominator field.
    * @return denominator
    */
   public long getDenominator() {
      return this.denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      return ""+Long.toString(this.numerator)+"/"+Long.toString(this.denominator);
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      Lfraction s = (Lfraction) m;
      return compareTo(s) == 0;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.numerator, this.denominator);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
      if (this.denominator == m.denominator) {
         return new Lfraction(this.numerator + m.numerator, this.denominator);
      } else {
         return new Lfraction((m.numerator*this.denominator)+(this.numerator*m.denominator), (m.denominator*this.denominator));
      }
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      return new Lfraction(this.numerator * m.numerator, this.denominator * m.denominator);
   }


   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (this.denominator == 0) {
         throw new RuntimeException("Division by 0 is prohibited");
      }
      else if (this.numerator < 1) {
         return new Lfraction(-this.denominator, -this.numerator);
      } else {
         return new Lfraction(this.denominator, this.numerator);
      }

   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-this.numerator, this.denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      if (this.denominator == m.denominator) {
         return new Lfraction(this.numerator - m.numerator, this.denominator);
      } else {
         return new Lfraction((this.numerator*m.denominator)-(m.numerator*this.denominator), (m.denominator*this.denominator));
      }
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.numerator == 0) {
         throw new RuntimeException("Division by 0 is prohibited");
      }
      long numerator = Math.abs(this.numerator * m.denominator);
      long denominator = Math.abs(this.denominator * m.numerator);
      return new Lfraction(numerator, denominator);
   }





   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      Lfraction temp = this.minus(m);
      if (temp.numerator > 0){
         return 1;
      } else if (temp.numerator < 0) {
         return -1;
      } else return 0;
   }


   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(this.numerator, this.denominator);
   }

   /** Integer part of the (improper) fraction.
    * @return integer part of this fraction
    */
   public long integerPart() {
      return this.numerator/this.denominator;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      if (this.numerator >= this.denominator || -this.numerator >= this.denominator) {
         return new Lfraction(this.numerator % this.denominator, this.denominator);
      } else {
         return new Lfraction(this.numerator, this.denominator);
      }
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double)this.numerator/this.denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      Lfraction ret = new Lfraction(0, 1);
      double math = Math.round(f * d);
      ret.numerator = (long) math;
      ret.denominator = d;
      return  ret;
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      if (s.isEmpty()) {
         throw new RuntimeException("Input is empty");
      }
      String[] parts = s.split("/");
      return new Lfraction(Long.parseLong(parts[0]), Long.parseLong(parts[1]));
   }

   public Lfraction pow(int n) {
      switch (n){
         case(0):
            return new Lfraction(1, 1);
         case (1):
            return new Lfraction(this.numerator, this.denominator);
         case (-1):
            return new Lfraction(this.numerator, this.denominator).inverse();
         default:
            if (n > 1) return this.times(this.pow(n - 1));
            else return this.pow(-n).inverse();
      }
   }

}